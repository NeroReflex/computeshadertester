#pragma once

#include "MessageQueue.h"

namespace Spark
{

	template <typename T>
	class ReadableMessageQueue : public MessageQueue<T>
	{
	public:
		ReadableMessageQueue() = default;
		virtual ~ReadableMessageQueue() = default;
		ReadableMessageQueue(const ReadableMessageQueue &) = delete;
		ReadableMessageQueue &operator=(const ReadableMessageQueue &) = delete;

		virtual std::shared_ptr<T> pop() noexcept = 0;
	};
}
