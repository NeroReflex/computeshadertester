#include <array>
#include <cassert>
#include <cstdlib>

#include "types.h"

bool checkForTriangleIndex(TreeNode *tree, TriangleReference *refs, glm::uint index, glm::uint triangleIndex) noexcept
{
    if (tree[index].leftIsLeaf)
    {
        if (refs[tree[index].left].index == triangleIndex)
            return true;
    }
    else
    {
        if (checkForTriangleIndex(tree, refs, tree[index].left, triangleIndex))
            return true;
    }

    if (tree[index].rightIsLeaf)
    {
        if (refs[tree[index].right].index == triangleIndex)
            return true;
    }
    else
    {
        if (checkForTriangleIndex(tree, refs, tree[index].right, triangleIndex))
            return true;
    }

    return false;
}

void visit(TreeNode *tree, TriangleReference *refs, glm::uint index) noexcept
{
    TreeNode cNode = tree[index];

    tree[index].visited = true;

    assert(!cNode.visited);

    if (tree[index].leftIsLeaf)
    {
        refs[tree[index].left].visited = true;
    }
    else
    {
        visit(tree, refs, tree[index].left);
    }

    if (tree[index].rightIsLeaf)
    {
        refs[tree[index].right].visited = true;
    }
    else
    {
        visit(tree, refs, tree[index].right);
    }
}

uint64_t treeDepth(TreeNode *tree, glm::uint index)
{
    if ((tree[index].leftIsLeaf) && (tree[index].rightIsLeaf))
    {
        return 1;
    }
    else if ((tree[index].leftIsLeaf) && (!tree[index].rightIsLeaf))
    {
        return 1 + treeDepth(tree, tree[index].right);
    }
    else if ((!tree[index].leftIsLeaf) && (tree[index].rightIsLeaf))
    {
        return 1 + treeDepth(tree, tree[index].left);
    }

    return 1 + glm::max(treeDepth(tree, tree[index].left), treeDepth(tree, tree[index].right));
}