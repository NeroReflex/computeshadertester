#pragma once

#include <iostream>
#include <mutex>
#include <vector>
#include <cassert>

#include "types.h"

#include "Task.hpp"

/*
// this is taken from Karras paper

// L = array of leaf nodes
// I array of internal nodes, where I0 is the root


1: for each internal node with index i ∈[0,n −2] in parallel
2: // Determine direction of the range (+1 or -1)
3: d ←sign(δ(i,i +1)−δ(i,i −1))
4: // Compute upper bound for the length of the range
5: δmin ←δ(i,i −d)
6: lmax ←2
7: while δ(i,i +lmax ·d) >δmin do
8: lmax ←lmax ·2
9: // Find the other end using binary search
10: l ←0
11: for t ←{lmax /2,lmax /4,...,1}do
12: if δ(i,i +(l +t)·d) >δmin then
13: l ←l +t
14: j ←i +l ·d
15: // Find the split position using binary search
16: δnode ←δ(i,j)
17: s ←0
18: for t ←{dl/2e,dl/4e,...,1}do
19: if δ(i,i +(s +t)·d) >δnode then
20: s ←s +t
21: γ←i +s ·d +min(d,0)
22: // Output child pointers
23: if min(i,j) = γthen left ←Lγelse left ←Iγ
24: if max(i,j) = γ+1 then right ←Lγ+1 else right ←Iγ+1
25: Ii ←(left,right)
26: end for
*/

template <uint32_t local_size_x, uint32_t local_size_y = 1, uint32_t local_size_z = 1>
class KarrasTask : virtual public Task<local_size_x, local_size_y, local_size_z>
{
public:
    KarrasTask() noexcept
        : Task<local_size_x, local_size_y, local_size_z>(),
          tree(nullptr), refs(nullptr)
    {
    }

    glm::ivec2 determineRange(const glm::int32 index_internal_nodes) const noexcept
    {
        const glm::int32 dleft = delta(index_internal_nodes, index_internal_nodes - 1);
        const glm::int32 dright = delta(index_internal_nodes, index_internal_nodes + 1);

        // Determine direction of the range (+1 or -1)
        const glm::int32 direction = glm::sign(dright - dleft);

        // Compute upper bound for the length of the range
        const glm::int32 delta_min = (direction == 1) ? dleft : dright;

        glm::int32 l_max = 2;

        while (delta(index_internal_nodes, index_internal_nodes + l_max * direction) > delta_min)
        {
            l_max *= 2;
        }

        glm::int32 l = 0;

        // Find the other end using binary search
        for (glm::int32 t = l_max / 2; t >= 1; t /= 2)
        {
            if (delta(index_internal_nodes, index_internal_nodes + (l + t) * direction) > delta_min)
            {
                l += t;
            }
        }

        glm::int32 j = index_internal_nodes + l * direction;

        return glm::ivec2(
            glm::min(index_internal_nodes, j),
            glm::max(index_internal_nodes, j));
    }

    glm::int32 findSplit(glm::int32 first, glm::int32 last)
    {
        const glm::int32 commonPrefix = delta(first, last);

        glm::int32 split = first;
        glm::int32 step = last - first;

        do
        {
            step = (step + 1) >> 1;      // exponential decrease
            int newSplit = split + step; // proposed new position

            if (newSplit < last)
            {
                // MortonType splitCode = sortedMortonCodes[newSplit];
                //  			int splitPrefix = __clz(firstCode ^ splitCode);
                int splitPrefix = delta(first, newSplit);
                if (splitPrefix > commonPrefix)
                    split = newSplit; // accept proposal
            }
        } while (step > 1);

        return split;
    }

    void
    operator()(glm::uvec3 gl_WorkGroupSize, glm::uvec3 gl_LocalInvocationID, glm::uvec3 gl_GlobalInvocationID) noexcept override
    {

        const glm::int32 index_internal_nodes = gl_GlobalInvocationID.x;

        if (index_internal_nodes >= (len - 1))
            return;

        const glm::ivec2 range = determineRange(index_internal_nodes);

        const glm::int32 first = range.x;
        const glm::int32 last = range.y;

        const glm::int32 split = findSplit(first, last);

        tree[index_internal_nodes].left = split;
        tree[index_internal_nodes].right = split + 1;
        tree[index_internal_nodes].rightIsLeaf = 1;
        tree[index_internal_nodes].leftIsLeaf = 1;

        // Output child pointers
        if (split != first)
        {
            assert(split != 0);
            tree[index_internal_nodes].leftIsLeaf = 0;
        }

        if ((split + 1) != last)
        {
            assert((split + 1) != 0);
            tree[index_internal_nodes].rightIsLeaf = 0;
        }
    }

    void bindIndexVertex(TriangleReference *refArray, uint32_t in_length)
    {
        refs = refArray;
        len = in_length;
    }

    void bindTree(TreeNode *nodes) noexcept
    {
        tree = nodes;
    }

private:
    static glm::int32 __clz(glm::uint x) noexcept
    {
        return glm::int32(31) - glm::int32(glm::log2<float>(x));
    }

    glm::int32 delta(glm::int32 i, glm::int32 j) const
    {
        if (((j < 0) || (j > len - 1)) || ((i < 0) || (i > len - 1)))
        {
            return -1;
        }

        assert(i >= 0);
        assert(j >= 0);
        assert(i < len);
        assert(j < len);

        // TODO: in standard GLSL we don't have uint64_t type!!!
        // maybe this is a fix:
        // #extension GL_EXT_shader_explicit_arithmetic_types_int64 : enable

        const uint64_t key_i = refs[i].morton;
        const uint64_t key_j = refs[j].morton;

        // 30-bit morton code
        assert((key_i & 0b1111111111111111111111111111111111000000000000000000000000000000) == 0);
        assert((key_j & 0b1111111111111111111111111111111111000000000000000000000000000000) == 0);

        const uint64_t composed_key_i = key_i << uint64_t(32) | uint64_t(i);
        const uint64_t composed_key_j = key_j << uint64_t(32) | uint64_t(j);

        const glm::uint64_t x = composed_key_i ^ composed_key_j;

        return glm::int32(63) - glm::int32(glm::log2<double>(x));
    }

    TreeNode *tree;

    TriangleReference *refs;

    uint32_t len;
};