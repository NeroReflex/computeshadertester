#pragma once

#include "types.h"

#include "Task.hpp"

#include "MultithreadMessageQueue.h"

#include <thread>
#include <atomic>
#include <barrier>
#include <semaphore>
#include <optional>
#include <iostream>
#include <barrier>
#include <functional>

template <size_t local_group_size>
class Compute
{

private:
    typedef std::barrier<std::function<void()>> barrier_t;

    class TaskMessage
    {
    public:
        virtual ~TaskMessage() = default;
    };

    class TaskTerminateMessage : virtual public TaskMessage
    {
    };

    class TaskExecuteMessage : virtual public TaskMessage
    {
    };

    static void
    task_code(
        std::shared_ptr<uint32_t> localGroupNumber,
        std::shared_ptr<std::optional<ExecutableTask *>> code,
        std::shared_ptr<Spark::MultithreadMessageQueue<TaskMessage>> messageQueue,
        std::shared_ptr<barrier_t> start_barrier,
        std::shared_ptr<std::counting_semaphore<local_group_size>> finish_semaphore,
        std::shared_ptr<glm::uvec3> WorkGroupID,
        uint32_t local_id)
    {
        do
        {
            const auto lastMsg = messageQueue->pop();

            if (std::dynamic_pointer_cast<TaskTerminateMessage>(lastMsg))
            {
                break;
            }
            else if (const auto executionMessage = std::dynamic_pointer_cast<TaskExecuteMessage>(lastMsg))
            {
                // wait for the pipeline to be laoded
                start_barrier->arrive_and_wait();

                if (code->has_value())
                {
                    const glm::uvec3 gl_WorkGroupSize = code->value()->getWorkGroupSize();

                    const glm::uint z = local_id / (gl_WorkGroupSize.x * gl_WorkGroupSize.y);
                    local_id -= (z * gl_WorkGroupSize.x * gl_WorkGroupSize.y);
                    const glm::uint y = local_id / gl_WorkGroupSize.x;
                    const glm::uint x = local_id % gl_WorkGroupSize.x;

                    const glm::uvec3 gl_LocalInvocationID(x, y, z);
                    const glm::uvec3 gl_WorkGroupID = *WorkGroupID;
                    const glm::uvec3 gl_GlobalInvocationID = gl_WorkGroupID * gl_WorkGroupSize + gl_LocalInvocationID;

                    (*(code->value()))(gl_WorkGroupSize, gl_LocalInvocationID, gl_GlobalInvocationID);
                }

                finish_semaphore->release();
            }

        } while (true);
    }

public:
    Compute() noexcept : mLocalGroupNumber(std::make_shared<uint32_t>(0)),
                         mTaskCode(std::make_shared<std::optional<ExecutableTask *>>()),
                         mTaskMessanger(),
                         mStartBarrier(std::make_shared<barrier_t>(local_group_size, []() {})),
                         mSemaphore(std::make_shared<std::counting_semaphore<local_group_size>>(0)),
                         mWorkGroupID(std::make_shared<glm::uvec3>(0.0, 0.0, 0.0))
    {
        for (auto &queue : mTaskMessanger)
        {
            queue.reset(new Spark::MultithreadMessageQueue<TaskMessage>());
        }

        uint32_t invocation_id = 0;
        for (auto &task_ptr : mTask)
        {
            task_ptr.reset(
                new std::thread(
                    task_code,
                    mLocalGroupNumber,
                    mTaskCode,
                    mTaskMessanger[invocation_id],
                    mStartBarrier,
                    mSemaphore,
                    mWorkGroupID,
                    invocation_id));
            task_ptr->detach();
            invocation_id++;
        }
    }

    template <uint32_t local_size_x, uint32_t local_size_y, uint32_t local_size_z>
    void dispatch(Task<local_size_x, local_size_y, local_size_z> *tsk, uint32_t num_x, uint32_t num_y = 1, uint32_t num_z = 1) noexcept
    {
        static_assert(local_size_x * local_size_y * local_size_z > 0, "Invalid dimensions for the local group");
        static_assert(local_size_x * local_size_y * local_size_z == local_group_size, "Invalid dimensions for the local group");

        *mLocalGroupNumber = 0;
        mTaskCode->emplace(tsk);

        for (uint32_t z = 0; z < num_z; ++z)
        {
            for (uint32_t y = 0; y < num_y; ++y)
            {
                for (uint32_t x = 0; x < num_x; ++x)
                {
                    *mWorkGroupID = glm::uvec3(x, y, z);

                    for (size_t k = 0; k < local_group_size; ++k)
                    {
                        mTaskMessanger[k]->push(std::shared_ptr<TaskMessage>(new TaskExecuteMessage()));
                    }

                    // Wait for local group to finish
                    for (size_t k = 0; k < local_group_size; ++k)
                    {
                        mSemaphore->acquire();
                    }

                    *mLocalGroupNumber += 1;
                }
            }
        }

        mTaskCode->reset();
    }

    inline virtual ~Compute()
    {
        for (size_t k = 0; k < local_group_size; ++k)
        {
            mTaskMessanger[k]->push(std::shared_ptr<TaskMessage>(new TaskTerminateMessage()));
        }
    }

private:
    std::shared_ptr<uint32_t> mLocalGroupNumber;

    std::shared_ptr<std::optional<ExecutableTask *>> mTaskCode;

    std::array<std::unique_ptr<std::thread>, local_group_size> mTask;

    std::array<std::shared_ptr<Spark::MultithreadMessageQueue<TaskMessage>>, local_group_size> mTaskMessanger;

    std::shared_ptr<barrier_t> mStartBarrier;

    std::shared_ptr<std::counting_semaphore<local_group_size>> mSemaphore;

    std::shared_ptr<glm::uvec3> mWorkGroupID;
};