#pragma once

#include <memory>

namespace Spark
{
	template <typename T>
	class MessageQueue
	{
	public:
		MessageQueue() = default;
		virtual ~MessageQueue() = default;

		MessageQueue(const MessageQueue &) = delete;
		MessageQueue(MessageQueue &&) = delete;
		MessageQueue &operator=(const MessageQueue &) = delete;
		MessageQueue &operator=(MessageQueue &&) = delete;

		virtual void push(const std::shared_ptr<T> &msg) noexcept = 0;
	};
}
