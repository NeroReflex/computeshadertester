#pragma once

#include <iostream>
#include <mutex>

#include "types.h"

#include "Task.hpp"

template <uint32_t local_group_size, uint32_t local_size_x, uint32_t local_size_y = 1, uint32_t local_size_z = 1>
class GlobalSortTask : virtual public Task<local_size_x, local_size_y, local_size_z>
{
public:
  GlobalSortTask() noexcept
      : Task<local_size_x, local_size_y, local_size_z>(),
        digit(0), in_data(nullptr), out_data(nullptr) {}

  void operator()(glm::uvec3 gl_WorkGroupSize, glm::uvec3 gl_LocalInvocationID, glm::uvec3 gl_GlobalInvocationID) noexcept override
  {
    // startIndex is INCLUDED
    const glm::uint startIndex =
        gl_GlobalInvocationID.x * max_num_of_elements_per_core;

    // endIndex is EXCLUDED
    const glm::uint endIndex = glm::min(startIndex + max_num_of_elements_per_core, len);

    if (startIndex >= endIndex)
    {
      return;
    }

    glm::uint up_to_this_point[2] = {0, 0};
    glm::uint up_to_the_end[2] = {0, 0};

    for (uint32_t i = 0; i < len; ++i)
    {
      const glm::uint convertedValueToUint = in_data[i].morton;

      const glm::uint one_or_zero = (convertedValueToUint >> digit) & 0x01;

      ++up_to_the_end[one_or_zero];
      if (i < startIndex)
      {
        ++up_to_this_point[one_or_zero];
      }
    }

    // to avoid a branch up_to_this_point[1] will also count the total number of zeroes!
    up_to_this_point[1] += up_to_the_end[0];

    for (uint32_t idx = startIndex; idx < endIndex; ++idx)
    {
      const auto convertedValueToUint = in_data[idx].morton;

      const glm::uint one_or_zero = (convertedValueToUint >> digit) & 0x01;

      out_data[up_to_this_point[one_or_zero]] = in_data[idx];

      up_to_this_point[one_or_zero]++;
    }
  }

  void bindIndexVertex(const TriangleReference *in_d, TriangleReference *out_d, uint32_t length)
  {
    out_data = out_d;
    in_data = in_d;
    len = length;
  }

  void setDigit(uint32_t in_digit) noexcept { digit = in_digit % 32; }

private:
  const TriangleReference *in_data;

  TriangleReference *out_data;

  uint32_t len;

  uint8_t digit;
};