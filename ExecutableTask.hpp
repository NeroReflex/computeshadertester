#pragma once

#include "types.h"

class ExecutableTask
{

public:
    ExecutableTask(
        uint32_t local_size_x,
        uint32_t local_size_y = 1,
        uint32_t local_size_z = 1) noexcept : mWorkGroupSize(local_size_x, local_size_y, local_size_z)
    {
    }

    virtual void operator()(glm::uvec3 gl_WorkGroupSize, glm::uvec3 gl_LocalInvocationID, glm::uvec3 gl_GlobalInvocationID) noexcept = 0;

    inline glm::uvec3 getWorkGroupSize() const noexcept { return mWorkGroupSize; }

protected:
    const glm::uvec3 mWorkGroupSize;
};