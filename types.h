#pragma once

#include <cstdlib>

#include <glm/glm.hpp>

// using namespace glm;

#define max_num_of_elements_per_core 1024

struct Triangle
{
    glm::float32 v1x;
    glm::float32 v1y;
    glm::float32 v1z;
    glm::float32 v2x;
    glm::float32 v2y;
    glm::float32 v2z;
    glm::float32 v3x;
    glm::float32 v3y;
    glm::float32 v3z;
};

struct AABB
{
    glm::float32 minX;
    glm::float32 minY;
    glm::float32 minZ;

    glm::float32 dimX;
    glm::float32 dimY;
    glm::float32 dimZ;
};

struct TreeNode
{
    AABB aabb;

    glm::uint left;
    glm::uint leftIsLeaf;

    glm::uint right;
    glm::uint rightIsLeaf;

    // this is only used for debugging
    glm::uint visited;
};

struct TriangleReference
{
    AABB aabb;

    glm::uint index;

    glm::uint morton;

    // this is only used for debugging
    glm::uint visited;
};

bool isAABBEmpty(AABB aabb)
{
    return aabb.minX + aabb.minY + aabb.minZ + aabb.dimX + aabb.dimY + aabb.dimZ == 0.0;
}

AABB createAABBFromTriangle(Triangle t)
{
    AABB result;

    result.minX = glm::min(glm::min(t.v1x, t.v2x), t.v3x);
    result.minY = glm::min(glm::min(t.v1y, t.v2y), t.v3y);
    result.minZ = glm::min(glm::min(t.v1z, t.v2z), t.v3z);

    const glm::float32 maxX = glm::max(glm::max(t.v1x, t.v2x), t.v3x);
    const glm::float32 maxY = glm::max(glm::max(t.v1y, t.v2y), t.v3y);
    const glm::float32 maxZ = glm::max(glm::max(t.v1z, t.v2z), t.v3z);

    result.dimX = maxX - result.minX;
    result.dimY = maxY - result.minY;
    result.dimZ = maxZ - result.minZ;

    return result;
}

AABB combineAABBs(AABB first, AABB second)
{
    AABB result;

    result.minX = glm::min(first.minX, second.minX);
    result.minY = glm::min(first.minY, second.minY);
    result.minZ = glm::min(first.minZ, second.minZ);

    const glm::float32 maxX = glm::max(first.minX + first.dimX, second.minX + second.dimX);
    const glm::float32 maxY = glm::max(first.minY + first.dimY, second.minY + second.dimY);
    const glm::float32 maxZ = glm::max(first.minZ + first.dimZ, second.minZ + second.dimZ);

    result.dimX = maxX - result.minX;
    result.dimY = maxY - result.minY;
    result.dimZ = maxZ - result.minZ;

    return result;
}
