#pragma once

#include "ReadableMessageQueue.h"

#include <deque>
#include <mutex>
#include <condition_variable>

namespace Spark
{

	template <typename T>
	class MultithreadMessageQueue : public ReadableMessageQueue<T>
	{
	public:
		MultithreadMessageQueue() = default;
		virtual ~MultithreadMessageQueue() = default;

		MultithreadMessageQueue(const MultithreadMessageQueue &) = delete;
		MultithreadMessageQueue &operator=(const MultithreadMessageQueue &) = delete;

		void push(const std::shared_ptr<T> &msg) noexcept override
		{
			std::unique_lock<std::mutex> lock(messagesAccessMutex);

			messages.push_back(msg);

			// Awake a waiting thread
			cv.notify_one();
		}

		std::shared_ptr<T> pop() noexcept override
		{
			auto result = std::shared_ptr<T>(nullptr);

			std::unique_lock<std::mutex> lock(messagesAccessMutex);

			// Avoid a busy-wait
			while (messages.empty())
				cv.wait(lock);

			result = messages.front();
			messages.pop_front();

			return result;
		}

	private:
		std::deque<std::shared_ptr<T>> messages;

		mutable std::mutex messagesAccessMutex;

		std::condition_variable cv;
	};
}
