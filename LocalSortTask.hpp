#pragma once

#include <iostream>
#include <mutex>
#include <vector>

#include "types.h"

#include "Task.hpp"

template <size_t local_group_size, uint32_t local_size_x, uint32_t local_size_y = 1, uint32_t local_size_z = 1>
class LocalSortTask : virtual public Task<local_size_x, local_size_y, local_size_z>
{
public:
  LocalSortTask() noexcept
      : Task<local_size_x, local_size_y, local_size_z>(),
        digit(0), refs(nullptr)
  {
  }

  void operator()(glm::uvec3 gl_WorkGroupSize, glm::uvec3 gl_LocalInvocationID, glm::uvec3 gl_GlobalInvocationID) noexcept override
  {
    // startIndex is INCLUDED
    const glm::uint startIndex =
        gl_GlobalInvocationID.x * max_num_of_elements_per_core;

    // endIndex is EXCLUDED
    const glm::uint endIndex = glm::min(startIndex + max_num_of_elements_per_core, len);

    if (startIndex >= endIndex)
    {
      return;
    }

    glm::uint idx = 0;

    TriangleReference bucket[2][max_num_of_elements_per_core];
    glm::uint bucket_cnt[] = {0, 0};

    glm::uint msb_set = static_cast<glm::uint>(0x01) << digit;

    // stuff with 0 goes to the bucket[number_of_zero_stuff_before][0] and stuff with 1 goes to bucket[number_of_one_stuff_before][1]
    for (idx = startIndex; idx < endIndex; ++idx)
    {
      const auto convertedValueToUint = refs[idx].morton;

      const auto one_or_zero = (convertedValueToUint & msb_set) >> digit;

      bucket[one_or_zero][bucket_cnt[one_or_zero]] = refs[idx];
      bucket_cnt[one_or_zero]++;
    }

    // The second step is to write out bucket by first outputting bucket[1..n][0] and then bucket[1..m][1] so that the sort is stable
    idx = 0;
    for (uint32_t c = 0; c < 2; ++c)
    {
      for (uint32_t k = 0; k < bucket_cnt[c]; ++k)
      {
        refs[startIndex + idx] = bucket[c][k];
        ++idx;
      }
    }
  }

  void bindIndexVertex(TriangleReference *refArray, uint32_t in_length)
  {
    refs = refArray;
    len = in_length;
  }

  void setDigit(uint32_t in_digit) noexcept { digit = in_digit % 32; }

private:
  TriangleReference *refs;

  uint32_t len;

  uint8_t digit;
};