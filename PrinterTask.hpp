#pragma once

#include "types.h"

#include <mutex>
#include <iostream>

#include "Compute.hpp"

template <size_t local_group_size, uint32_t local_size_x, uint32_t local_size_y = 1, uint32_t local_size_z = 1>
class PrinterTask : virtual public Task<local_size_x, local_size_y, local_size_z>
{
public:
    PrinterTask() noexcept : Task<local_size_x, local_size_y, local_size_z>() {}

    void operator()(glm::uvec3 gl_WorkGroupSize, glm::uvec3 gl_LocalInvocationID, glm::uvec3 gl_GlobalInvocationID) noexcept override
    {
        std::lock_guard<std::mutex> lck(mMutex);
        std::cout << "uvec3(" << gl_GlobalInvocationID.x << ", " << gl_GlobalInvocationID.y << ", " << gl_GlobalInvocationID.z << ")" << std::endl;
    }

private:
    std::mutex mMutex;
};