#pragma once

#include "ExecutableTask.hpp"

template <uint32_t local_size_x, uint32_t local_size_y, uint32_t local_size_z>
class Task : public ExecutableTask
{
public:
    Task() noexcept : ExecutableTask(local_size_x, local_size_y, local_size_z) {}
};