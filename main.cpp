#include <array>
#include <cassert>
#include <cstdlib>
#include <iostream>
#include <random>

#include "InitializationTask.hpp"
#include "GlobalSortTask.hpp"
#include "LocalSortTask.hpp"
#include "PrinterTask.hpp"
#include "KarrasTask.hpp"
#include "Compute.hpp"

#include "CPUAlgorithms.hpp"

#include <chrono>
using namespace std::chrono;
using namespace std;

#define number_of_cores_in_local_group 16

static constexpr size_t len = 10000;

int main()
{

  const size_t octree_levels = ceil(log(len) / log(8));
  const size_t nearest_power_of_eight_of_len = pow(8, octree_levels);

  // Popolo l'array di numeri casuali
  auto primitives = std::make_shared<std::array<Triangle, len>>();
  auto in = std::make_shared<std::array<TriangleReference, len>>();
  auto out = std::make_shared<std::array<TriangleReference, len>>();

  std::cout << "the next power of eight for the number " << len << " is " << nearest_power_of_eight_of_len << " therefore to store all of the elements an octree " << octree_levels << "-deep is needed" << std::endl;

  size_t bytes = len * sizeof(Triangle);

  std::cout
      << "In memory the array of elements is " << (bytes / 1024) / 1024 << " MiB large" << std::endl;

  std::cout
      << "Maximum supported number of elements: "
      << std::numeric_limits<uint32_t>::max() << "; actually using " << len
      << std::endl;

  std::cout << "Filling the array with random numbers..." << std::endl;

  unsigned seed = chrono::system_clock::now().time_since_epoch().count();
  default_random_engine generator(seed);
  normal_distribution<double> distribution(0.0, 1.0);

  for (size_t r = 0; r < len; ++r)
  {
    Triangle randomTriangle;
    randomTriangle.v1x = distribution(generator);
    randomTriangle.v1y = distribution(generator);
    randomTriangle.v1z = distribution(generator);
    randomTriangle.v2x = distribution(generator);
    randomTriangle.v2y = distribution(generator);
    randomTriangle.v2z = distribution(generator);
    randomTriangle.v3x = distribution(generator);
    randomTriangle.v3y = distribution(generator);
    randomTriangle.v3z = distribution(generator);

    (*primitives)[r] = randomTriangle;

    assert(!isAABBEmpty(createAABBFromTriangle(randomTriangle)));
  }
  std::cout << "Array has been filled with random numbers!" << std::endl;

  std::cout << "Setting up Compute shader emulator..." << std::endl;

  Compute<number_of_cores_in_local_group> compute_shader_emulator;

  std::cout << "Compute shader emulator ready!" << std::endl;

  InitializationTask<number_of_cores_in_local_group> initializator;
  initializator.bindGeomtryData(primitives->data());
  initializator.bindIndexVertex(in->data(), len);

  compute_shader_emulator.dispatch(
      &initializator,
      std::ceil(float(len) / float(number_of_cores_in_local_group)));

  /*
  PrinterTask<32> printer(max_num_of_elements_per_core, 1, 1);
  compute_shader_emulator.dispatch(&printer, std::ceil(float(len) /
  float(max_num_of_elements_per_core)));
  */

  std::cout << "Sorting " << len << " elements... Execution time will be measured!" << std::endl;

  auto start = high_resolution_clock::now();

  /*
    PrinterTask<number_of_cores_in_local_group, number_of_cores_in_local_group> debug;
    compute_shader_emulator.dispatch(
        &debug,
        std::ceil(float(len) / float(max_num_of_elements_per_core)));
  */

  // as we are using  30-bits morton code it's not necessary to sort by all 32-bits, but only on less significant 30s!
  for (uint32_t digit = 0; digit < 30; ++digit)
  {
    LocalSortTask<number_of_cores_in_local_group, number_of_cores_in_local_group> localSorter;
    localSorter.bindIndexVertex(in->data(), len);
    localSorter.setDigit(digit);
    compute_shader_emulator.dispatch(
        &localSorter,
        std::ceil(float(len) / float(max_num_of_elements_per_core)));

    GlobalSortTask<number_of_cores_in_local_group, number_of_cores_in_local_group> globalSorter;
    globalSorter.bindIndexVertex(in->data(), out->data(), len);
    globalSorter.setDigit(digit);
    compute_shader_emulator.dispatch(
        &globalSorter,
        std::ceil(float(len) / float(max_num_of_elements_per_core)));

    std::swap(in, out);
  }

  // after this the final result will be in "in" buffer (as after any even number of iterations).

  auto stop = high_resolution_clock::now();

  auto duration = duration_cast<seconds>(stop - start);
  cout << "Sorting " << len << " elements took " << duration.count()
       << " seconds" << endl;

  // controllo il risultato

  uint32_t minimum_number = std::numeric_limits<uint32_t>::max();
  uint32_t maximum_number = std::numeric_limits<uint32_t>::min();

  bool is_fully_ordered = true;
  for (size_t k = 1; k < len; ++k)
  {
    const auto first = (*in)[k - 1].morton;
    const auto second = (*in)[k].morton;

    minimum_number = (first < minimum_number) ? first : minimum_number;
    minimum_number = (second < minimum_number) ? second : minimum_number;
    maximum_number = (first > maximum_number) ? first : maximum_number;
    maximum_number = (second > maximum_number) ? second : maximum_number;

    if (first > second)
    {
      is_fully_ordered = false;
      break;
    }
  }

  if (is_fully_ordered)
  {
    std::cout << "The sequence of " << len << " is now fully ordered. Minimum number: " << minimum_number << ", maximum number: " << maximum_number << std::endl;
  }
  else
  {
    std::cerr << "The sequence of " << len << " is not fully ordered. Probably I have a bug in my implementation!" << std::endl;
    return EXIT_FAILURE;
  }

  auto tree = std::make_shared<std::array<TreeNode, len - 1>>();

  KarrasTask<number_of_cores_in_local_group> bvhBuilder;
  bvhBuilder.bindIndexVertex(in->data(), len);
  bvhBuilder.bindTree(tree->data());

  start = high_resolution_clock::now();

  compute_shader_emulator.dispatch(
      &bvhBuilder,
      std::ceil(float(len) / float(number_of_cores_in_local_group)));

  stop = high_resolution_clock::now();

  duration = duration_cast<seconds>(stop - start);
  cout << "Constructing the BVH radix tree for " << len << " elements took " << duration.count()
       << " seconds" << endl;

  visit(tree->data(), in->data(), 0);

  bool is_fully_visitable = true;
  bool every_triangle_is_referenced = checkForTriangleIndex(tree->data(), in->data(), 0, 0);

  std::cout << "Checking the resulting BVH tree..." << std::endl;

  std::cout << "[";
  std::cout.flush();

  for (size_t k = 1; (k < len) && (is_fully_visitable) && (every_triangle_is_referenced); ++k)
  {
    if (!checkForTriangleIndex(tree->data(), in->data(), 0, k))
    {
      every_triangle_is_referenced = false;
      std::cerr << "Geometry with index " << k << " cannot be visited!" << std::endl;
      break;
    } else {
      if ((k % 10000) == 0) {
        std::cout << "=";
        std::cout.flush();
      }
    }

    if (!(*in)[k].visited)
    {
      is_fully_visitable = false;
      std::cerr << "Geometry reference at index " << k << " cannot be visited!" << std::endl;
      break;
    }
  }

  std::cout << "]" << std::endl;
  std::cout.flush();

  if ((is_fully_visitable) && (every_triangle_is_referenced))
  {
    std::cout << std::endl
              << "The BVH is fully traversable and is " << treeDepth(tree->data(), 0) << " levels deep" << std::endl;
  }
  else
  {
    std::cerr << "The BVH is not fully taversable. Probably I have a bug in my implementation!" << std::endl;
    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}