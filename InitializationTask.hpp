#pragma once

#include <iostream>
#include <mutex>
#include <vector>
#include <cassert>

#include "types.h"

#include "Task.hpp"

template <uint32_t local_size_x, uint32_t local_size_y = 1, uint32_t local_size_z = 1>
class InitializationTask : virtual public Task<local_size_x, local_size_y, local_size_z>
{
public:
    InitializationTask() noexcept
        : Task<local_size_x, local_size_y, local_size_z>(),
          geometry(nullptr), refs(nullptr)
    {
    }

    void operator()(glm::uvec3 gl_WorkGroupSize, glm::uvec3 gl_LocalInvocationID, glm::uvec3 gl_GlobalInvocationID) noexcept override
    {

        const glm::int32 index_internal_nodes = gl_GlobalInvocationID.x;

        if (index_internal_nodes > (len - 1))
            return;

        // at the beginning each reference at index i references the triangle at index i
        refs[index_internal_nodes].index = index_internal_nodes;

        // visited will serve us later in aabb population, to make sure both childs have a valid aabb
        refs[index_internal_nodes].visited = 0;

        // this is used to reduce acc to geometry, trading RAM random access time and execution time with space
        refs[index_internal_nodes].morton = convertTriangleCenterToUint(geometry[refs[index_internal_nodes].index]);

        // this will be used when I'll have time to optimize the tree with triangles
        refs[index_internal_nodes].aabb = createAABBFromTriangle(geometry[refs[index_internal_nodes].index]);
    }

    void bindIndexVertex(TriangleReference *refArray, uint32_t in_length)
    {
        refs = refArray;
        len = in_length;
    }

    void bindGeomtryData(const Triangle *const in) noexcept
    {
        geometry = in;
    }

private:

    // Expands a 10-bit integer into 30 bits
    // by inserting 2 zeros after each bit.
    static glm::uint expandBits(glm::uint v)
    {
        v = (v * 0x00010001u) & 0xFF0000FFu;
        v = (v * 0x00000101u) & 0x0F00F00Fu;
        v = (v * 0x00000011u) & 0xC30C30C3u;
        v = (v * 0x00000005u) & 0x49249249u;
        return v;
    }

    // Calculates a 30-bit Morton code for the
    // given 3D point located within the unit cube [0,1].
    static glm::uint morton3D(glm::float32 x, glm::float32 y, glm::float32 z)
    {
        x = glm::min(glm::max(x * 1024.0f, 0.0f), 1023.0f);
        y = glm::min(glm::max(y * 1024.0f, 0.0f), 1023.0f);
        z = glm::min(glm::max(z * 1024.0f, 0.0f), 1023.0f);
        glm::uint xx = expandBits((glm::uint)x);
        glm::uint yy = expandBits((glm::uint)y);
        glm::uint zz = expandBits((glm::uint)z);
        return xx * 4 + yy * 2 + zz;
    }

    /**
     * This function requires the input node to be a triangle.
     */
    static glm::uint convertTriangleCenterToUint(Triangle triangle)
    {
        glm::float32 avg_x = (triangle.v1x + triangle.v2x + triangle.v3x) / 3.0;
        glm::float32 avg_y = (triangle.v1y + triangle.v2y + triangle.v3y) / 3.0;
        glm::float32 avg_z = (triangle.v1z + triangle.v2z + triangle.v3z) / 3.0;

        return morton3D(avg_x, avg_y, avg_z);
    }


    const Triangle *geometry;

    TriangleReference *refs;

    uint32_t len;
};